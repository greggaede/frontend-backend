<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Skill;
use AppBundle\Entity\Repository\SkillRepository;
use AppBundle\Form\Type\SkillType;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Class SkillsController
 * @package AppBundle\Controller
 *
 * @RouteResource("skill")
 */
class SkillsController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Gets an individual Skill
     *
     * @param int $id
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @ApiDoc(
     *     output="AppBundle\Entity\Skill",
     *     statusCodes={
     *         200 = "Returned when successful",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function getAction($id)
    {
        $skill = $this->getSkillRepository()->createFindOneByIdQuery($id)->getSingleResult();
        
        if ($skill === null) {
            return new View(null, Response::HTTP_NOT_FOUND);
        }
        
        return $skill;
    }

    /**
     * Gets a collection of Skills
     *
     * @return array
     *
     * @ApiDoc(
     *     output="AppBundle\Entity\Skill",
     *     statusCodes={
     *         200 = "Returned when successful",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function cgetAction()
    {
        return $this->getSkillRepository()->createFindAllQuery()->getResult();
    }

    /**
     * @param Request $request
     * @return View|\Symfony\Component\Form\Form
     *
     * @ApiDoc(
     *     input="AppBundle\Form\Type\SkillType",
     *     output="AppBundle\Entity\Skill",
     *     statusCodes={
     *         201 = "Returned when a new Skill has been successfully created",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function postAction(Request $request)
    {
        $form = $this->createForm(SkillType::class, null, [
            'csrf_protection' => false,        
        ]);
        
        $form->submit($request->request->all());
        
        if (!$form->isValid()) {
            return $form;
        }

        /**
         * @var $skill Skill
         */
        $skill = $form->getData();
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($skill);
        $em->flush();

        $routeOptions = [
            'id' => $skill->getId(),
            '_format' => $request->get('_format'),
        ];

        return $this->routeRedirectView('get_skill', $routeOptions, Response::HTTP_CREATED);
    }

    /**
     * @param Request $request
     * @param int     $id
     * @return View|\Symfony\Component\Form\Form
     *
     * @ApiDoc(
     *     input="AppBundle\Form\Type\SkillType",
     *     output="AppBundle\Entity\Skill",
     *     statusCodes={
     *         204 = "Returned when an existing Skill has been successful updated",
     *         400 = "Return when errors",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function putAction(Request $request, $id)
    {
        /**
         * @var $skill Skill
         */
        $skill = $this->getSkillRepository()->find($id);

        if ($skill === null) {
            return new View(null, Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(SkillType::class, $skill, [
            'csrf_protection' => false,
        ]);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $form;
        }

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $routeOptions = [
            'id' => $skill->getId(),
            '_format' => $request->get('_format'),
        ];

        return $this->routeRedirectView('get_skill', $routeOptions, Response::HTTP_NO_CONTENT);
    }


    /**
     * @param Request $request
     * @param int     $id
     * @return View|\Symfony\Component\Form\Form
     *
     * @ApiDoc(
     *     input="AppBundle\Form\Type\SkillType",
     *     output="AppBundle\Entity\Skill",
     *     statusCodes={
     *         204 = "Returned when an existing Skill has been successfully updated",
     *         400 = "Return when errors",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function patchAction(Request $request, $id)
    {
        /**
         * @var $skill Skill
         */
        $skill = $this->getSkillRepository()->find($id);

        if ($skill === null) {
            return new View(null, Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(SkillType::class, $skill, [
            'csrf_protection' => false,
        ]);

        $form->submit($request->request->all(), false);

        if (!$form->isValid()) {
            return $form;
        }

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $routeOptions = [
            'id' => $skill->getId(),
            '_format' => $request->get('_format'),
        ];

        return $this->routeRedirectView('get_skill', $routeOptions, Response::HTTP_NO_CONTENT);
    }


    /**
     * @param int $id
     * @return View
     *
     * @ApiDoc(
     *     statusCodes={
     *         204 = "Returned when an existing Skill has been successfully deleted",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function deleteAction($id)
    {
        /**
         * @var $skill Skill
         */
        $skill = $this->getSkillRepository()->find($id);

        if ($skill === null) {
            return new View(null, Response::HTTP_NOT_FOUND);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($skill);
        $em->flush();

        return new View(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @return SkillRepository
     */
    private function getSkillRepository()
    {
        return $this->get('crv.doctrine_entity_repository.skill');
    }
}
