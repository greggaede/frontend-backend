<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Monster;
use AppBundle\Entity\Repository\MonsterRepository;
use AppBundle\Form\Type\MonsterType;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Class MonstersController
 * @package AppBundle\Controller
 *
 * @RouteResource("monster")
 */
class MonstersController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Gets an individual Monster
     *
     * @param int $id
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @ApiDoc(
     *     output="AppBundle\Entity\Monster",
     *     statusCodes={
     *         200 = "Returned when successful",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function getAction($id)
    {
        $monster = $this->getMonsterRepository()->createFindOneByIdQuery($id)->getSingleResult();
        
        if ($monster === null) {
            return new View(null, Response::HTTP_NOT_FOUND);
        }
        
        return $monster;
    }

    /**
     * Gets a collection of Monsters
     *
     * @return array
     *
     * @ApiDoc(
     *     output="AppBundle\Entity\Monster",
     *     statusCodes={
     *         200 = "Returned when successful",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function cgetAction()
    {
        return $this->getMonsterRepository()->createFindAllQuery()->getResult();
    }

    /**
     * @param Request $request
     * @return View|\Symfony\Component\Form\Form
     *
     * @ApiDoc(
     *     input="AppBundle\Form\Type\MonsterType",
     *     output="AppBundle\Entity\Monster",
     *     statusCodes={
     *         201 = "Returned when a new Monster has been successfully created",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function postAction(Request $request)
    {
        $form = $this->createForm(MonsterType::class, null, [
            'csrf_protection' => false,        
        ]);
        
        $form->submit($request->request->all());
        
        if (!$form->isValid()) {
            return $form;
        }

        /**
         * @var $monster Monster
         */
        $monster = $form->getData();
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($monster);
        $em->flush();

        $routeOptions = [
            'id' => $monster->getId(),
            '_format' => $request->get('_format'),
        ];

        return $this->routeRedirectView('get_monster', $routeOptions, Response::HTTP_CREATED);
    }

    /**
     * @param Request $request
     * @param int     $id
     * @return View|\Symfony\Component\Form\Form
     *
     * @ApiDoc(
     *     input="AppBundle\Form\Type\MonsterType",
     *     output="AppBundle\Entity\Monster",
     *     statusCodes={
     *         204 = "Returned when an existing Monster has been successful updated",
     *         400 = "Return when errors",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function putAction(Request $request, $id)
    {
        /**
         * @var $monster Monster
         */
        $monster = $this->getMonsterRepository()->find($id);

        if ($monster === null) {
            return new View(null, Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(MonsterType::class, $monster, [
            'csrf_protection' => false,
        ]);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $form;
        }

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $routeOptions = [
            'id' => $monster->getId(),
            '_format' => $request->get('_format'),
        ];

        return $this->routeRedirectView('get_monster', $routeOptions, Response::HTTP_NO_CONTENT);
    }


    /**
     * @param Request $request
     * @param int     $id
     * @return View|\Symfony\Component\Form\Form
     *
     * @ApiDoc(
     *     input="AppBundle\Form\Type\MonsterType",
     *     output="AppBundle\Entity\Monster",
     *     statusCodes={
     *         204 = "Returned when an existing Monster has been successfully updated",
     *         400 = "Return when errors",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function patchAction(Request $request, $id)
    {
        /**
         * @var $monster Monster
         */
        $monster = $this->getMonsterRepository()->find($id);

        if ($monster === null) {
            return new View(null, Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(MonsterType::class, $monster, [
            'csrf_protection' => false,
        ]);

        $form->submit($request->request->all(), false);

        if (!$form->isValid()) {
            return $form;
        }

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $routeOptions = [
            'id' => $monster->getId(),
            '_format' => $request->get('_format'),
        ];

        return $this->routeRedirectView('get_monster', $routeOptions, Response::HTTP_NO_CONTENT);
    }


    /**
     * @param int $id
     * @return View
     *
     * @ApiDoc(
     *     statusCodes={
     *         204 = "Returned when an existing Monster has been successfully deleted",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function deleteAction($id)
    {
        /**
         * @var $monster Monster
         */
        $monster = $this->getMonsterRepository()->find($id);

        if ($monster === null) {
            return new View(null, Response::HTTP_NOT_FOUND);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($monster);
        $em->flush();

        return new View(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @return MonsterRepository
     */
    private function getMonsterRepository()
    {
        return $this->get('crv.doctrine_entity_repository.monster');
    }
}
