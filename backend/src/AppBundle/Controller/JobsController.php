<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Job;
use AppBundle\Entity\Repository\JobRepository;
use AppBundle\Form\Type\JobType;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Class JobsController
 * @package AppBundle\Controller
 *
 * @RouteResource("job")
 */
class JobsController extends FOSRestController implements ClassResourceInterface
{
    /**
     * Gets an individual Job
     *
     * @param int $id
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @ApiDoc(
     *     output="AppBundle\Entity\Job",
     *     statusCodes={
     *         200 = "Returned when successful",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function getAction($id)
    {
        $job = $this->getJobRepository()->createFindOneByIdQuery($id)->getSingleResult();
        
        if ($job === null) {
            return new View(null, Response::HTTP_NOT_FOUND);
        }
        
        return $job;
    }

    /**
     * Gets a collection of Jobs
     *
     * @return array
     *
     * @ApiDoc(
     *     output="AppBundle\Entity\Job",
     *     statusCodes={
     *         200 = "Returned when successful",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function cgetAction()
    {
        return $this->getJobRepository()->createFindAllQuery()->getResult();
    }

    /**
     * @param Request $request
     * @return View|\Symfony\Component\Form\Form
     *
     * @ApiDoc(
     *     input="AppBundle\Form\Type\JobType",
     *     output="AppBundle\Entity\Job",
     *     statusCodes={
     *         201 = "Returned when a new Job has been successfully created",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function postAction(Request $request)
    {
        $form = $this->createForm(JobType::class, null, [
            'csrf_protection' => false,        
        ]);
        
        $form->submit($request->request->all());
        
        if (!$form->isValid()) {
            return $form;
        }

        /**
         * @var $job Job
         */
        $job = $form->getData();
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($job);
        $em->flush();

        $routeOptions = [
            'id' => $job->getId(),
            '_format' => $request->get('_format'),
        ];

        return $this->routeRedirectView('get_job', $routeOptions, Response::HTTP_CREATED);
    }

    /**
     * @param Request $request
     * @param int     $id
     * @return View|\Symfony\Component\Form\Form
     *
     * @ApiDoc(
     *     input="AppBundle\Form\Type\JobType",
     *     output="AppBundle\Entity\Job",
     *     statusCodes={
     *         204 = "Returned when an existing Job has been successful updated",
     *         400 = "Return when errors",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function putAction(Request $request, $id)
    {
        /**
         * @var $job Job
         */
        $job = $this->getJobRepository()->find($id);

        if ($job === null) {
            return new View(null, Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(JobType::class, $job, [
            'csrf_protection' => false,
        ]);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $form;
        }

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $routeOptions = [
            'id' => $job->getId(),
            '_format' => $request->get('_format'),
        ];

        return $this->routeRedirectView('get_job', $routeOptions, Response::HTTP_NO_CONTENT);
    }


    /**
     * @param Request $request
     * @param int     $id
     * @return View|\Symfony\Component\Form\Form
     *
     * @ApiDoc(
     *     input="AppBundle\Form\Type\JobType",
     *     output="AppBundle\Entity\Job",
     *     statusCodes={
     *         204 = "Returned when an existing Job has been successfully updated",
     *         400 = "Return when errors",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function patchAction(Request $request, $id)
    {
        /**
         * @var $job Job
         */
        $job = $this->getJobRepository()->find($id);

        if ($job === null) {
            return new View(null, Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(JobType::class, $job, [
            'csrf_protection' => false,
        ]);

        $form->submit($request->request->all(), false);

        if (!$form->isValid()) {
            return $form;
        }

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $routeOptions = [
            'id' => $job->getId(),
            '_format' => $request->get('_format'),
        ];

        return $this->routeRedirectView('get_job', $routeOptions, Response::HTTP_NO_CONTENT);
    }


    /**
     * @param int $id
     * @return View
     *
     * @ApiDoc(
     *     statusCodes={
     *         204 = "Returned when an existing Job has been successfully deleted",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function deleteAction($id)
    {
        /**
         * @var $job Job
         */
        $job = $this->getJobRepository()->find($id);

        if ($job === null) {
            return new View(null, Response::HTTP_NOT_FOUND);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($job);
        $em->flush();

        return new View(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @return JobRepository
     */
    private function getJobRepository()
    {
        return $this->get('crv.doctrine_entity_repository.job');
    }
}
