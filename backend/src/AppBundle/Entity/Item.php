<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMSSerializer;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\ItemRepository")
 * @ORM\Table(name="items")
 * @JMSSerializer\ExclusionPolicy("all")
 */
class Item implements \JsonSerializable
{
  /**
   * @ORM\Column(type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   * @JMSSerializer\Expose
   */
  protected $id;

  /**
   * @ORM\Column(type="string", name="name")
   * @JMSSerializer\Expose
   */
  private $name;

  /**
   * @ORM\Column(type="string", name="description")
   * @JMSSerializer\Expose
   */
  private $description;

  /**
   * @ORM\Column(type="integer", name="hp_raise")
   * @JMSSerializer\Expose
   */
  private $hp_raise;

  /**
   * @ORM\Column(type="integer", name="mp_raise")
   * @JMSSerializer\Expose
   */
  private $mp_raise;

  /**
   * @ORM\Column(type="integer", name="atk_raise")
   * @JMSSerializer\Expose
   */
  private $atk_raise;

  /**
   * @ORM\Column(type="integer", name="def_raise")
   * @JMSSerializer\Expose
   */
  private $def_raise;


  /**
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @return mixed
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * @param mixed $name
   * @return Item
   */
  public function setName($name)
  {
    $this->name = $name;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * @param mixed $description
   * @return Item
   */
  public function setDescription($description)
  {
    $this->description = $description;

    return $this;
  }

  /**
   * @return int
   */
  public function getHPRaise()
  {
    return $this->hp_raise;
  }

  /**
   * @param int $hp_raise
   * @return Item
   */
  public function setHPRaise($hp_raise)
  {
    $this->hp_raise = $hp_raise;

    return $this;
  }

  /**
   * @return int
   */
  public function getMPRaise()
  {
    return $this->mp_raise;
  }

  /**
   * @param int $mp_raise
   * @return Item
   */
  public function setMPRaise($mp_raise)
  {
    $this->mp_raise = $mp_raise;

    return $this;
  }

  /**
   * @return int
   */
  public function getATKRaise()
  {
    return $this->atk_raise;
  }

  /**
   * @param int $atk_raise
   * @return Item
   */
  public function setATKRaise($atk_raise)
  {
    $this->atk_raise = $atk_raise;

    return $this;
  }

  /**
   * @return int
   */
  public function getDEFRaise()
  {
    return $this->def_raise;
  }

  /**
   * @param int $def_raise
   * @return Item
   */
  public function setDEFRaise($def_raise)
  {
    $this->def_raise = $def_raise;

    return $this;
  }

  /**
   * @return mixed
   */
  function jsonSerialize()
  {
    return [
      'id'    => $this->id,
      'name' => $this->name,
      'description' => $this->description,
      'hp_raise'  => $this->hp_raise,
      'mp_raise'  => $this->mp_raise,
      'atk_raise'  => $this->atk_raise,
      'def_raise'  => $this->def_raise,
    ];
  }

}