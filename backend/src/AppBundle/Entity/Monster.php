<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMSSerializer;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\MonsterRepository")
 * @ORM\Table(name="monsters")
 * @JMSSerializer\ExclusionPolicy("all")
 */
class Monster implements \JsonSerializable
{
  /**
   * @ORM\Column(type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   * @JMSSerializer\Expose
   */
  protected $id;

  /**
   * @ORM\Column(type="string", name="name")
   * @JMSSerializer\Expose
   */
  private $name;

  /**
   * @ORM\Column(type="integer", name="hp")
   * @JMSSerializer\Expose
   */
  private $hp;

  /**
   * @ORM\Column(type="integer", name="mp")
   * @JMSSerializer\Expose
   */
  private $mp;

  /**
   * @ORM\Column(type="integer", name="atk")
   * @JMSSerializer\Expose
   */
  private $atk;

  /**
   * @ORM\Column(type="integer", name="def")
   * @JMSSerializer\Expose
   */
  private $def;


  /**
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @return mixed
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * @param mixed $name
   * @return Monster
   */
  public function setName($name)
  {
    $this->name = $name;

    return $this;
  }

  /**
   * @return int
   */
  public function getHP()
  {
    return $this->hp;
  }

  /**
   * @param int $hp
   * @return Monster
   */
  public function setHP($hp)
  {
    $this->hp = $hp;

    return $this;
  }

  /**
   * @return int
   */
  public function getMP()
  {
    return $this->mp;
  }

  /**
   * @param int $mp
   * @return Monster
   */
  public function setMP($mp)
  {
    $this->mp = $mp;

    return $this;
  }

  /**
   * @return int
   */
  public function getATK()
  {
    return $this->atk;
  }

  /**
   * @param int $atk
   * @return Monster
   */
  public function setATK($atk)
  {
    $this->atk = $atk;

    return $this;
  }

  /**
   * @return int
   */
  public function getDEF()
  {
    return $this->def;
  }

  /**
   * @param int $def
   * @return Monster
   */
  public function setDEF($def)
  {
    $this->def = $def;

    return $this;
  }

  /**
   * @return mixed
   */
  function jsonSerialize()
  {
    return [
      'id'    => $this->id,
      'name' => $this->name,
      'hp'  => $this->hp,
      'mp'  => $this->mp,
      'atk'  => $this->atk,
      'def'  => $this->def,
    ];
  }

}