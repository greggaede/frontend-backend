<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMSSerializer;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\SkillRepository")
 * @ORM\Table(name="skills")
 * @JMSSerializer\ExclusionPolicy("all")
 */
class Skill implements \JsonSerializable
{
  /**
   * @ORM\Column(type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   * @JMSSerializer\Expose
   */
  protected $id;

  /**
   * @ORM\Column(type="string", name="name")
   * @JMSSerializer\Expose
   */
  private $name;

  /**
   * @ORM\Column(type="string", name="description")
   * @JMSSerializer\Expose
   */
  private $description;

  /**
   * @ORM\Column(type="integer", name="hp_cost")
   * @JMSSerializer\Expose
   */
  private $hp_cost;

  /**
   * @ORM\Column(type="integer", name="mp_cost")
   * @JMSSerializer\Expose
   */
  private $mp_cost;

  /**
   * @ORM\Column(type="integer", name="atk_cost")
   * @JMSSerializer\Expose
   */
  private $atk_cost;

  /**
   * @ORM\Column(type="integer", name="def_cost")
   * @JMSSerializer\Expose
   */
  private $def_cost;

  /**
   * @ORM\Column(type="integer", name="hp_dmg")
   * @JMSSerializer\Expose
   */
  private $hp_dmg;

  /**
   * @ORM\Column(type="integer", name="mp_dmg")
   * @JMSSerializer\Expose
   */
  private $mp_dmg;

  /**
   * @ORM\Column(type="integer", name="atk_dmg")
   * @JMSSerializer\Expose
   */
  private $atk_dmg;

  /**
   * @ORM\Column(type="integer", name="def_dmg")
   * @JMSSerializer\Expose
   */
  private $def_dmg;


  /**
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @return mixed
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * @param mixed $name
   * @return Skill
   */
  public function setName($name)
  {
    $this->name = $name;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * @param mixed $description
   * @return Skill
   */
  public function setDescription($description)
  {
    $this->description = $description;

    return $this;
  }

  /**
   * @return int
   */
  public function getHPCost()
  {
    return $this->hp_cost;
  }

  /**
   * @param int $hp_cost
   * @return Skill
   */
  public function setHPCost($hp_cost)
  {
    $this->hp_cost = $hp_cost;

    return $this;
  }

  /**
   * @return int
   */
  public function getMPCost()
  {
    return $this->mp_cost;
  }

  /**
   * @param int $mp_cost
   * @return Skill
   */
  public function setMPCost($mp_cost)
  {
    $this->mp_cost = $mp_cost;

    return $this;
  }

  /**
   * @return int
   */
  public function getATKCost()
  {
    return $this->atk_cost;
  }

  /**
   * @param int $atk_cost
   * @return Skill
   */
  public function setATKCost($atk_cost)
  {
    $this->atk_cost = $atk_cost;

    return $this;
  }

  /**
   * @return int
   */
  public function getDEFCost()
  {
    return $this->def_cost;
  }

  /**
   * @param int $def_cost
   * @return Skill
   */
  public function setDEFCost($def_cost)
  {
    $this->def_cost = $def_cost;

    return $this;
  }

  /**
   * @return int
   */
  public function getHPDmg()
  {
    return $this->hp_dmg;
  }

  /**
   * @param int $hp_dmg
   * @return Skill
   */
  public function setHPDmg($hp_dmg)
  {
    $this->hp_dmg = $hp_dmg;

    return $this;
  }

  /**
   * @return int
   */
  public function getMPDmg()
  {
    return $this->mp_dmg;
  }

  /**
   * @param int $mp_dmg
   * @return Skill
   */
  public function setMPDmg($mp_dmg)
  {
    $this->mp_dmg = $mp_dmg;

    return $this;
  }

  /**
   * @return int
   */
  public function getATKDmg()
  {
    return $this->atk_dmg;
  }

  /**
   * @param int $atk_dmg
   * @return Skill
   */
  public function setATKDmg($atk_dmg)
  {
    $this->atk_dmg = $atk_dmg;

    return $this;
  }

  /**
   * @return int
   */
  public function getDEFDmg()
  {
    return $this->def_dmg;
  }

  /**
   * @param int $def_dmg
   * @return Skill
   */
  public function setDEFDmg($def_dmg)
  {
    $this->def_dmg = $def_dmg;

    return $this;
  }

  /**
   * @return mixed
   */
  function jsonSerialize()
  {
    return [
      'id'    => $this->id,
      'name' => $this->name,
      'description' => $this->description,
      'hp_dmg'  => $this->hp_dmg,
      'mp_dmg'  => $this->mp_dmg,
      'atk_dmg'  => $this->atk_dmg,
      'def_dmg'  => $this->def_dmg,
    ];
  }

}