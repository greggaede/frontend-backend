<?php

namespace AppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class SkillRepository extends EntityRepository
{
    public function createFindAllQuery()
    {
        return $this->_em->createQuery(
            "
            SELECT bp
            FROM AppBundle:Skill bp
            "
        );
    }


    public function createFindOneByIdQuery($id)
    {
        $query = $this->_em->createQuery(
            "
            SELECT bp
            FROM AppBundle:Skill bp
            WHERE bp.id = :id
            "
        );

        $query->setParameter('id', $id);

        return $query;
    }
}