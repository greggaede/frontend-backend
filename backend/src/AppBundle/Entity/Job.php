<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMSSerializer;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\JobRepository")
 * @ORM\Table(name="jobs")
 * @JMSSerializer\ExclusionPolicy("all")
 */
class Job implements \JsonSerializable
{
  /**
   * @ORM\Column(type="integer")
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="AUTO")
   * @JMSSerializer\Expose
   */
  protected $id;

  /**
   * @ORM\Column(type="string", name="name")
   * @JMSSerializer\Expose
   */
  private $name;

  /**
   * @ORM\Column(type="string", name="description")
   * @JMSSerializer\Expose
   */
  private $description;

  /**
   * @ORM\Column(type="integer", name="hp")
   * @JMSSerializer\Expose
   */
  private $hp;

  /**
   * @ORM\Column(type="integer", name="mp")
   * @JMSSerializer\Expose
   */
  private $mp;

  /**
   * @ORM\Column(type="integer", name="atk")
   * @JMSSerializer\Expose
   */
  private $atk;

  /**
   * @ORM\Column(type="integer", name="def")
   * @JMSSerializer\Expose
   */
  private $def;


  /**
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @return mixed
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * @param mixed $name
   * @return Job
   */
  public function setName($name)
  {
    $this->name = $name;

    return $this;
  }

  /**
   * @return mixed
   */
  public function getDescription()
  {
    return $this->description;
  }

  /**
   * @param mixed $description
   * @return Job
   */
  public function setDescription($description)
  {
    $this->description = $description;

    return $this;
  }

  /**
   * @return int
   */
  public function getHP()
  {
    return $this->hp;
  }

  /**
   * @param int $hp
   * @return Job
   */
  public function setHP($hp)
  {
    $this->hp = $hp;

    return $this;
  }

  /**
   * @return int
   */
  public function getMP()
  {
    return $this->mp;
  }

  /**
   * @param int $mp
   * @return Job
   */
  public function setMP($mp)
  {
    $this->mp = $mp;

    return $this;
  }

  /**
   * @return int
   */
  public function getATK()
  {
    return $this->atk;
  }

  /**
   * @param int $atk
   * @return Job
   */
  public function setATK($atk)
  {
    $this->atk = $atk;

    return $this;
  }

  /**
   * @return int
   */
  public function getDEF()
  {
    return $this->def;
  }

  /**
   * @param int $def
   * @return Job
   */
  public function setDEF($def)
  {
    $this->def = $def;

    return $this;
  }

  /**
   * @return mixed
   */
  function jsonSerialize()
  {
    return [
      'id'    => $this->id,
      'name' => $this->name,
      'description' => $this->description,
      'hp'  => $this->hp,
      'mp'  => $this->mp,
      'atk'  => $this->atk,
      'def'  => $this->def,
    ];
  }

}