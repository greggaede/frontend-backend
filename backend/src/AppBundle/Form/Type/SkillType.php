<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SkillType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('description', TextType::class)
            ->add('hp_cost', TextType::class)
            ->add('mp_cost', TextType::class)
            ->add('atk_cost', TextType::class)
            ->add('def_cost', TextType::class)
            ->add('hp_dmg', TextType::class)
            ->add('mp_dmg', TextType::class)
            ->add('atk_dmg', TextType::class)
            ->add('def_dmg', TextType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Skill',
            'allow_extra_fields' => true,
        ]);
    }

    public function getName()
    {
        return 'skill';
    }
}