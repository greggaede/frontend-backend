import React, {Component} from 'react';
import {Router, browserHistory, Route, IndexRoute, withRouter} from 'react-router'
import TitleScreen from './containers/title-screen';
import AdminIndex from './containers/admin';
import ListMonsters from './containers/monsters/list';
import CreateMonster from './containers/monsters/create';
import UpdateMonster from './containers/monsters/update';

import ListJobs from './containers/jobs/list';
import CreateJob from './containers/jobs/create';
import UpdateJob from './containers/jobs/update';

import ListItems from './containers/items/list';
import CreateItem from './containers/items/create';
import UpdateItem from './containers/items/update';

import ListSkills from './containers/skills/list';
import CreateSkill from './containers/skills/create';
import UpdateSkill from './containers/skills/update';

import NewGame from './containers/new-game/index';

import NotFoundPage from './components/NotFoundPage';
import RollStats from "./containers/roll-stats";

export default class App extends Component {

    render() {
        return (
            <Router history={browserHistory}>
                <Route path="/" component={withRouter(TitleScreen)}/>
                <Route path="/admin" component={withRouter(AdminIndex)}/>
                <Route path="/admin/monsters" component={withRouter(ListMonsters)}/>
                <Route path="/admin/monsters/create" component={withRouter(CreateMonster)}/>
                <Route path="/admin/monsters/update/:monsterId" component={withRouter(UpdateMonster)}/>

                <Route path="/admin/jobs" component={withRouter(ListJobs)}/>
                <Route path="/admin/jobs/create" component={withRouter(CreateJob)}/>
                <Route path="/admin/jobs/update/:jobId" component={withRouter(UpdateJob)}/>

                <Route path="/admin/items" component={withRouter(ListItems)}/>
                <Route path="/admin/items/create" component={withRouter(CreateItem)}/>
                <Route path="/admin/items/update/:itemId" component={withRouter(UpdateItem)}/>

                <Route path="/admin/skills" component={withRouter(ListSkills)}/>
                <Route path="/admin/skills/create" component={withRouter(CreateSkill)}/>
                <Route path="/admin/skills/update/:skillId" component={withRouter(UpdateSkill)}/>

                <Route path="/new-game" component={withRouter(NewGame)}/>
                <Route path="/roll-stats" component={withRouter(RollStats)}/>

                <Route path="*" component={NotFoundPage}/>
            </Router>
        );
    }
}
