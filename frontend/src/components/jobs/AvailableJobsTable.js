import React, {Component} from 'react';
import {Link} from 'react-router';
import Cookies from 'universal-cookie';

let selectedJob = "";
const cookies = new Cookies();

export default class AvailableJobsTable extends Component {

  constructor(props) {
    super(props);
    this.state = {
      active: false
    };
    this.makeActive = this.makeActive.bind(this);
  };

  jobSelectHandler(e) {
    //TODO: Add an option to save these things in the DB
    cookies.set('RPG Class', selectedJob['id'], { path: '/', maxAge: '157680000' });
  }

  makeActive (i) {
    this.setState({i});
    selectedJob = this.props.jobs[i];
  }

  render() {
    return (
        <div>
          <table className="table table-hover table-responsive">
            <thead>
            <tr>
              <th>Name</th>
              <th>Description</th>
              <th>Base Stats</th>
            </tr>
            </thead>
            <tbody>

            {this.props.jobs && this.props.jobs.map((job, i) => {
              return (
                  <tr key={job.id} className={this.state.i === i ? "selected" : ""} onClick={this.makeActive.bind(this, i)}>
                    <td>{job.name}</td>
                    <td>{job.description}</td>
                    <td>HP: {job.hp}, MP: {job.mp}, ATK: {job.atk},
                      DEF: {job.def}</td>
                  </tr>);
            })}
            </tbody>
          </table>

          <Link to="/roll-stats" onClick={this.jobSelectHandler.bind(this)}
               className="btn btn-lg btn-success">Select class
          </Link>
        </div>
    );
  }

}