import React, { Component } from 'react';

const MonsterForm = React.createClass({

    getInitialState() {
        return {
            name: this.props.name || 'New Monster',
            hp: this.props.hp || 1,
            mp: this.props.mp || 0,
            atk: this.props.atk || 1,
            def: this.props.def || 0,
        }
    },

    componentWillReceiveProps(props) {
        this.setState(props);
    },

    handleNameChange(e) {
        this.setState({
            name: e.target.value
        });
    },

    handleHpChange(e) {
        this.setState({
            hp: Number(e.target.value)
        });
    },

    handleMpChange(e) {
        this.setState({
            mp: Number(e.target.value)
        });
    },

    handleAtkChange(e) {
        this.setState({
            atk: Number(e.target.value)
        });
    },

    handleDefChange(e) {
        this.setState({
            def: Number(e.target.value)
        });
    },

    handleSubmit(e) {
        e.preventDefault();
        this.props.onSubmit(this.state);
    },

    render() {
        return (
            <form name="monster" className="form-horizontal" onSubmit={this.handleSubmit}>
                <div id="monster">
                    <div className="form-group">
                        <label className="col-sm-2 control-label required" htmlFor="monster_name">Name</label>
                        <div className="col-sm-10">
                            <input type="text"
                                   id="monster_name"
                                   required="required"
                                   value={this.state.name}
                                   onChange={this.handleNameChange}
                                   className="form-control"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required" htmlFor="monster_hp">HP</label>
                        <div className="col-sm-10">
                            <input type="text"
                                   id="monster_hp"
                                   required="required"
                                   value={this.state.hp}
                                   onChange={this.handleHpChange}
                                   className="form-control"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required" htmlFor="monster_mp">MP</label>
                        <div className="col-sm-10">
                            <input type="text"
                                   id="monster_mp"
                                   required="required"
                                   value={this.state.mp}
                                   onChange={this.handleMpChange}
                                   className="form-control"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required" htmlFor="monster_atk">ATK</label>
                        <div className="col-sm-10">
                            <input type="text"
                                   id="monster_atk"
                                   required="required"
                                   value={this.state.atk}
                                   onChange={this.handleAtkChange}
                                   className="form-control"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required" htmlFor="monster_def">DEF</label>
                        <div className="col-sm-10">
                            <input type="text"
                                   id="monster_def"
                                   required="required"
                                   value={this.state.def}
                                   onChange={this.handleDefChange}
                                   className="form-control"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="col-sm-10 col-sm-offset-2">
                            <button type="submit"
                                    id="monster_submit"
                                    className="btn-default btn">
                                Submit
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        );
    }
});

export default MonsterForm;