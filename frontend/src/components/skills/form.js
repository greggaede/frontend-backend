import React, { Component } from 'react';

const SkillForm = React.createClass({

    getInitialState() {
        return {
            name: this.props.name || 'New Skill',
            description: this.props.description || 'A new skill.',
            hp_cost: this.props.hp_cost || 0,
            mp_cost: this.props.mp_cost || 2,
            atk_cost: this.props.atk_cost || 0,
            def_cost: this.props.def_cost || 0,
            hp_dmg: this.props.hp_dmg || 8,
            mp_dmg: this.props.mp_dmg || 0,
            atk_dmg: this.props.atk_dmg || 0,
            def_dmg: this.props.def_dmg || 0,
        }
    },

    componentWillReceiveProps(props) {
        this.setState(props);
    },

    handleNameChange(e) {
        this.setState({
            name: e.target.value
        });
    },

    handleDescriptionChange(e) {
        this.setState({
            description: e.target.value
        });
    },

    handleHpCostChange(e) {
        this.setState({
            hp_cost: Number(e.target.value)
        });
    },

    handleMpCostChange(e) {
        this.setState({
            mp_cost: Number(e.target.value)
        });
    },

    handleAtkCostChange(e) {
        this.setState({
            atk_cost: Number(e.target.value)
        });
    },

    handleDefCostChange(e) {
        this.setState({
            def_cost: Number(e.target.value)
        });
    },

    handleHpChange(e) {
        this.setState({
            hp_dmg: Number(e.target.value)
        });
    },

    handleMpChange(e) {
        this.setState({
            mp_dmg: Number(e.target.value)
        });
    },

    handleAtkChange(e) {
        this.setState({
            atk_dmg: Number(e.target.value)
        });
    },

    handleDefChange(e) {
        this.setState({
            def_dmg: Number(e.target.value)
        });
    },

    handleSubmit(e) {
        e.preventDefault();
        this.props.onSubmit(this.state);
    },

    render() {
        return (
            <form name="skill" className="form-horizontal" onSubmit={this.handleSubmit}>
                <div id="skill">
                    <div className="form-group">
                        <label className="col-sm-2 control-label required" htmlFor="skill_name">Name</label>
                        <div className="col-sm-10">
                            <input type="text"
                                   id="skill_name"
                                   required="required"
                                   value={this.state.name}
                                   onChange={this.handleNameChange}
                                   className="form-control"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required" htmlFor="skill_description">Description</label>
                        <div className="col-sm-10">
                            <input type="text"
                                   id="skill_description"
                                   required="required"
                                   value={this.state.description}
                                   onChange={this.handleDescriptionChange}
                                   className="form-control"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required" htmlFor="skill_hp_cost">HP Cost</label>
                        <div className="col-sm-10">
                            <input type="text"
                                   id="skill_hp_cost"
                                   required="required"
                                   value={this.state.hp_cost}
                                   onChange={this.handleHpCostChange}
                                   className="form-control"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required" htmlFor="skill_mp_cost">MP Cost</label>
                        <div className="col-sm-10">
                            <input type="text"
                                   id="skill_mp_cost"
                                   required="required"
                                   value={this.state.mp_cost}
                                   onChange={this.handleMpCostChange}
                                   className="form-control"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required" htmlFor="skill_atk_cost">ATK Cost</label>
                        <div className="col-sm-10">
                            <input type="text"
                                   id="skill_atk_cost"
                                   required="required"
                                   value={this.state.atk_cost}
                                   onChange={this.handleAtkCostChange}
                                   className="form-control"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required" htmlFor="skill_def_cost">DEF Cost</label>
                        <div className="col-sm-10">
                            <input type="text"
                                   id="skill_def_cost"
                                   required="required"
                                   value={this.state.def_cost}
                                   onChange={this.handleDefCostChange}
                                   className="form-control"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required" htmlFor="skill_hp_dmg">HP Damage</label>
                        <div className="col-sm-10">
                            <input type="text"
                                   id="skill_hp_dmg"
                                   required="required"
                                   value={this.state.hp_dmg}
                                   onChange={this.handleHpChange}
                                   className="form-control"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required" htmlFor="skill_mp_dmg">MP Damage</label>
                        <div className="col-sm-10">
                            <input type="text"
                                   id="skill_mp_dmg"
                                   required="required"
                                   value={this.state.mp_dmg}
                                   onChange={this.handleMpChange}
                                   className="form-control"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required" htmlFor="skill_atk_dmg">ATK Damage</label>
                        <div className="col-sm-10">
                            <input type="text"
                                   id="skill_atk_dmg"
                                   required="required"
                                   value={this.state.atk_dmg}
                                   onChange={this.handleAtkChange}
                                   className="form-control"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required" htmlFor="skill_def_dmg">DEF Damage</label>
                        <div className="col-sm-10">
                            <input type="text"
                                   id="skill_def_dmg"
                                   required="required"
                                   value={this.state.def_dmg}
                                   onChange={this.handleDefChange}
                                   className="form-control"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="col-sm-10 col-sm-offset-2">
                            <button type="submit"
                                    id="skill_submit"
                                    className="btn-default btn">
                                Submit
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        );
    }
});

export default SkillForm;