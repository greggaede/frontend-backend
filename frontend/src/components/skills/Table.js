import React, { Component } from 'react';
import { Link } from 'react-router';

export default class SkillTable extends Component {

    constructor(props) {
        super(props);
    };

    deleteHandler(i, e) {
        e.preventDefault();
        this.props.onDelete(this.props.skills[i].id);
    };

    render() {
        return (
            <div>
                <table className="table table-hover table-responsive">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Options</th>
                    </tr>
                    </thead>
                    <tbody>

                    {this.props.skills && this.props.skills.map((skill, i) => {
                        return (
                            <tr key={skill.id}>
                                <td>{skill.id}</td>
                                <td>{skill.name}</td>
                                <td>
                                    <Link to={`/admin/skills/update/${skill.id}`} className="btn btn-default btn-sm">Edit</Link>
                                    <btn onClick={this.deleteHandler.bind(this, i)} className="btn btn-danger btn-sm">Delete</btn>
                                </td>
                            </tr>);
                    })}
                    </tbody>
                </table>

                <Link to="/admin/skills/create" className="btn btn-lg btn-success">Create</Link>
                <Link to="/admin" className="btn btn-lg btn-danger">Back</Link>
            </div>
        );
    }

}