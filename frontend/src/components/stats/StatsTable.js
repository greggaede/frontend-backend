import React, {Component} from 'react';
import {Link} from 'react-router';
import Cookies from 'universal-cookie';
import {getRandomInt} from '../../actions/generalActions';

let finalStats = 'Stats not rolled!';
const cookies = new Cookies();

export default class StatsTable extends Component {

    constructor(props) {
        super(props);

        this.state = {
            rolls: [0, 0, 0, 0]
        };
    };

    statRollHandler(e) {
        this.setState(state => {
            state.rolls = [getRandomInt(7), getRandomInt(7), getRandomInt(7), getRandomInt(7)];
            return state;
        })
    }

    keepStatsHandler(e) {
        finalStats = 'HP.' + (this.props.stats[0] + this.state.rolls[0]) + '-MP.' + (this.props.stats[1] + this.state.rolls[1]) + '-ATK.' + (this.props.stats[2] + this.state.rolls[2]) + '-DEF.' + (this.props.stats[3] + this.state.rolls[3]);
        cookies.set('RPG Stats', finalStats, {path: '/', maxAge: '157680000'});
    }

    render() {
        return (
            <div>
                <table className="table table-responsive">
                    <thead>
                    <tr>
                        <th>Stats</th>
                        <th>Base</th>
                        <th>Plus</th>
                        <th>TOTAL</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            HP
                        </td>
                        <td>
                            {this.props.stats[0]}
                        </td>
                        <td>
                            +{this.state.rolls[0]}
                        </td>
                        <td>
                            {this.props.stats[0] + this.state.rolls[0]}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            MP
                        </td>
                        <td>
                            {this.props.stats[1]}
                        </td>
                        <td>
                            +{this.state.rolls[1]}
                        </td>
                        <td>
                            {this.props.stats[1] + this.state.rolls[1]}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ATK
                        </td>
                        <td>
                            {this.props.stats[2]}
                        </td>
                        <td>
                            +{this.state.rolls[2]}
                        </td>
                        <td>
                            {this.props.stats[2] + this.state.rolls[2]}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            DEF
                        </td>
                        <td>
                            {this.props.stats[3]}
                        </td>
                        <td>
                            +{this.state.rolls[3]}
                        </td>
                        <td>
                            {this.props.stats[3] + this.state.rolls[3]}
                        </td>
                    </tr>
                    </tbody>
                </table>

                <Link onClick={this.statRollHandler.bind(this)}
                      className="btn btn-lg btn-success">Roll stats
                </Link>
                <Link to="/character-name" onClick={this.keepStatsHandler.bind(this)}
                      className="btn btn-lg btn-warning">Keep stats
                </Link>
            </div>
        );
    }

}