import React, { Component } from 'react';

const ItemForm = React.createClass({

    getInitialState() {
        return {
            name: this.props.name || 'New Item',
            description: this.props.description || 'A new item.',
            hp_raise: this.props.hp_raise || 5,
            mp_raise: this.props.mp_raise || 0,
            atk_raise: this.props.atk_raise || 0,
            def_raise: this.props.def_raise || 0,
        }
    },

    componentWillReceiveProps(props) {
        this.setState(props);
    },

    handleNameChange(e) {
        this.setState({
            name: e.target.value
        });
    },

    handleDescriptionChange(e) {
        this.setState({
            description: e.target.value
        });
    },

    handleHpChange(e) {
        this.setState({
            hp_raise: Number(e.target.value)
        });
    },

    handleMpChange(e) {
        this.setState({
            mp_raise: Number(e.target.value)
        });
    },

    handleAtkChange(e) {
        this.setState({
            atk_raise: Number(e.target.value)
        });
    },

    handleDefChange(e) {
        this.setState({
            def_raise: Number(e.target.value)
        });
    },

    handleSubmit(e) {
        e.preventDefault();
        this.props.onSubmit(this.state);
    },

    render() {
        return (
            <form name="item" className="form-horizontal" onSubmit={this.handleSubmit}>
                <div id="item">
                    <div className="form-group">
                        <label className="col-sm-2 control-label required" htmlFor="item_name">Name</label>
                        <div className="col-sm-10">
                            <input type="text"
                                   id="item_name"
                                   required="required"
                                   value={this.state.name}
                                   onChange={this.handleNameChange}
                                   className="form-control"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required" htmlFor="item_description">Description</label>
                        <div className="col-sm-10">
                            <input type="text"
                                   id="item_description"
                                   required="required"
                                   value={this.state.description}
                                   onChange={this.handleDescriptionChange}
                                   className="form-control"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required" htmlFor="item_hp_raise">HP Raised</label>
                        <div className="col-sm-10">
                            <input type="text"
                                   id="item_hp_raise"
                                   required="required"
                                   value={this.state.hp_raise}
                                   onChange={this.handleHpChange}
                                   className="form-control"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required" htmlFor="item_mp_raise">MP Raised</label>
                        <div className="col-sm-10">
                            <input type="text"
                                   id="item_mp_raise"
                                   required="required"
                                   value={this.state.mp_raise}
                                   onChange={this.handleMpChange}
                                   className="form-control"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required" htmlFor="item_atk_raise">ATK Raised</label>
                        <div className="col-sm-10">
                            <input type="text"
                                   id="item_atk_raise"
                                   required="required"
                                   value={this.state.atk_raise}
                                   onChange={this.handleAtkChange}
                                   className="form-control"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="col-sm-2 control-label required" htmlFor="item_def_raise">DEF Raised</label>
                        <div className="col-sm-10">
                            <input type="text"
                                   id="item_def_raise"
                                   required="required"
                                   value={this.state.def_raise}
                                   onChange={this.handleDefChange}
                                   className="form-control"/>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="col-sm-10 col-sm-offset-2">
                            <button type="submit"
                                    id="item_submit"
                                    className="btn-default btn">
                                Submit
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        );
    }
});

export default ItemForm;