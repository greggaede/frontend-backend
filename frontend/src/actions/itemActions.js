import fetch from 'isomorphic-fetch';

export function fetchItems() {
    return fetch('http://127.0.0.1:8000/items', {
        method: 'GET',
        mode: 'CORS'
    }).then(res => res.json())
    .catch(err => err);
}

export function fetchItem(id) {
    return fetch('http://127.0.0.1:8000/items/' + id, {
        method: 'GET',
        mode: 'CORS'
    }).then(res => res.json())
    .catch(err => err);
}

export function createItem(data) {
    return fetch('http://127.0.0.1:8000/items', {
        method: 'POST',
        mode: 'CORS',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => {
        return res;
    }).catch(err => err);
}

export function updateItem(id, data) {
    return fetch('http://127.0.0.1:8000/items/' + id, {
        method: 'PUT',
        mode: 'CORS',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => {
        return res;
    }).catch(err => err);
}

export function deleteItem(id) {
    return fetch('http://127.0.0.1:8000/items/' + id, {
        method: 'DELETE',
        mode: 'CORS'
    }).then(res => {
        return res;
    }).catch(err => err);
}

