import fetch from 'isomorphic-fetch';

export function fetchJobs() {
    return fetch('http://127.0.0.1:8000/jobs', {
        method: 'GET',
        mode: 'CORS'
    }).then(res => res.json())
    .catch(err => err);
}

export function fetchJob(id) {
    return fetch('http://127.0.0.1:8000/jobs/' + id, {
        method: 'GET',
        mode: 'CORS'
    }).then(res => res.json())
    .catch(err => err);
}

export function createJob(data) {
    return fetch('http://127.0.0.1:8000/jobs', {
        method: 'POST',
        mode: 'CORS',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => {
        return res;
    }).catch(err => err);
}

export function updateJob(id, data) {
    return fetch('http://127.0.0.1:8000/jobs/' + id, {
        method: 'PUT',
        mode: 'CORS',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => {
        return res;
    }).catch(err => err);
}

export function deleteJob(id) {
    return fetch('http://127.0.0.1:8000/jobs/' + id, {
        method: 'DELETE',
        mode: 'CORS'
    }).then(res => {
        return res;
    }).catch(err => err);
}

