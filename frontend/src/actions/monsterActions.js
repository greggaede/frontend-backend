import fetch from 'isomorphic-fetch';

export function fetchMonsters() {
    return fetch('http://127.0.0.1:8000/monsters', {
        method: 'GET',
        mode: 'CORS'
    }).then(res => res.json())
    .catch(err => err);
}

export function fetchMonster(id) {
    return fetch('http://127.0.0.1:8000/monsters/' + id, {
        method: 'GET',
        mode: 'CORS'
    }).then(res => res.json())
    .catch(err => err);
}

export function createMonster(data) {
    return fetch('http://127.0.0.1:8000/monsters', {
        method: 'POST',
        mode: 'CORS',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => {
        return res;
    }).catch(err => err);
}

export function updateMonster(id, data) {
    return fetch('http://127.0.0.1:8000/monsters/' + id, {
        method: 'PUT',
        mode: 'CORS',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => {
        return res;
    }).catch(err => err);
}

export function deleteMonster(id) {
    return fetch('http://127.0.0.1:8000/monsters/' + id, {
        method: 'DELETE',
        mode: 'CORS'
    }).then(res => {
        return res;
    }).catch(err => err);
}

