import fetch from 'isomorphic-fetch';

export function fetchSkills() {
    return fetch('http://127.0.0.1:8000/skills', {
        method: 'GET',
        mode: 'CORS'
    }).then(res => res.json())
    .catch(err => err);
}

export function fetchSkill(id) {
    return fetch('http://127.0.0.1:8000/skills/' + id, {
        method: 'GET',
        mode: 'CORS'
    }).then(res => res.json())
    .catch(err => err);
}

export function createSkill(data) {
    return fetch('http://127.0.0.1:8000/skills', {
        method: 'POST',
        mode: 'CORS',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => {
        return res;
    }).catch(err => err);
}

export function updateSkill(id, data) {
    return fetch('http://127.0.0.1:8000/skills/' + id, {
        method: 'PUT',
        mode: 'CORS',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => {
        return res;
    }).catch(err => err);
}

export function deleteSkill(id) {
    return fetch('http://127.0.0.1:8000/skills/' + id, {
        method: 'DELETE',
        mode: 'CORS'
    }).then(res => {
        return res;
    }).catch(err => err);
}

