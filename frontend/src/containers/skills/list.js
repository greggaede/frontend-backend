import React, { Component } from 'react';
import {fetchSkills, deleteSkill} from '../../actions/skillActions';
import SkillTable from '../../components/skills/Table';

export default class ListSkills extends Component {

    constructor(props) {
        super(props);

        this.state = {
            skills: []
        };
    };

    componentDidMount() {
        fetchSkills()
            .then((data) => {
                this.setState(state => {
                    state.skills = data;
                    return state;
                })
            })
            .catch((err) => {
                console.error('err', err);
            });
    };

    onDelete(id) {
        deleteSkill(id)
            .then((data) => {
                let skills = this.state.skills.filter((skill) => {
                    return id !== skill.id;
                });

                this.setState(state => {
                    state.skills = skills;
                    return state;
                });
            })
            .catch((err) => {
                console.error('err', err);
            });
    }

    render() {
        return (
            <div>
                <SkillTable skills={this.state.skills}
                       onDelete={this.onDelete.bind(this)}
                />
            </div>
        );
    }
}
