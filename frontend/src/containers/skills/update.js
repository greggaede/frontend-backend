import React, { Component } from 'react';
import SkillForm from '../../components/skills/form';
import { fetchSkill, updateSkill } from '../../actions/skillActions';

const UpdateSkill = React.createClass ({

    getInitialState() {
        return {
            skill: {}
        };
    },

    componentDidMount() {
        fetchSkill(this.props.params.skillId)
            .then((data) => {
                this.setState(state => {
                    state.skill = data;
                    return state;
                });
            })
            .catch((err) => {
                console.error('err', err);
            });
    },

    handleSubmit(data) {
        updateSkill(this.state.skill.id, data);
        this.props.router.push('/admin/skills');
    },

    render() {
        return (
            <div>
                <SkillForm onSubmit={this.handleSubmit}
                      name={this.state.skill.name}
                      description={this.state.skill.description}
                      hp_cost={this.state.skill.hp_cost}
                      mp_cost={this.state.skill.mp_cost}
                      atk_cost={this.state.skill.atk_cost}
                      def_cost={this.state.skill.def_cost}
                      hp_dmg={this.state.skill.hp_dmg}
                      mp_dmg={this.state.skill.mp_dmg}
                      atk_dmg={this.state.skill.atk_dmg}
                      def_dmg={this.state.skill.def_dmg}>
                </SkillForm>
            </div>
        );
    }
});

export default UpdateSkill;
