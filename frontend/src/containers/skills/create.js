import React, { Component } from 'react';
import { withRouter } from 'react-router';
import SkillForm from '../../components/skills/form';
import { createSkill } from '../../actions/skillActions';

export default class CreateSkill extends Component {

    handleSubmit(data) {
        this.props.router.push('/admin/skills');
        createSkill(data);
    }

    render() {
        return (
            <div>
                <SkillForm onSubmit={this.handleSubmit.bind(this)}></SkillForm>
            </div>
        );
    }
}
