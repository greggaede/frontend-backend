import React, { Component } from 'react';

export default class TitleScreen extends Component {

    render() {
        return (
            <div className="titleScreen">
                <h1>COLLISEUM RPG</h1>
                <div className="col-xs-12">
                    <ul>
                        <li>
                            <a href="/new-game" className="btn btn-lg btn-success">New Game</a>
                        </li>
                        <li>
                            <a href="/continue" className="btn btn-lg btn-success">Continue Game</a>
                        </li>
                        <li>
                            <a href="/options" className="btn btn-lg btn-success">Options</a>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}