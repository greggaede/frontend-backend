import React, { Component } from 'react';

export default class AdminIndex extends Component {

    render() {
        return (
            <div className="adminIndex">
                <ul>
                    <li>
                        <a href="/admin/monsters" className="btn btn-lg btn-success">Monster DB</a>
                    </li>
                    <li>
                        <a href="/admin/items" className="btn btn-lg btn-success">Item DB</a>
                    </li>
                    <li>
                        <a href="/admin/jobs" className="btn btn-lg btn-success">Class DB</a>
                    </li>
                    <li>
                        <a href="/admin/skills" className="btn btn-lg btn-success">Skill DB</a>
                    </li>
                </ul>
            </div>
        );
    }
}