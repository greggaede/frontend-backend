import React, { Component } from 'react';
import ListStats from './listStats';

export default class RollStats extends Component {

    render() {
        return (
            <div className="newGame">
                <h1 className="text-center">New Game</h1>
                <h2>Roll your stats:</h2>
                <ListStats />
            </div>
        );
    }
}