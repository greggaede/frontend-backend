import React, { Component } from 'react';
import {fetchJob} from '../../actions/jobActions';
import StatsTable from '../../components/stats/StatsTable';
import Cookies from 'universal-cookie';

const cookies = new Cookies();

export default class ListStats extends Component {

    constructor(props) {
        super(props);

        this.state = {
            stats: []
        };
    };

    componentDidMount() {
        const savedJob = cookies.get('RPG Class');

        fetchJob(savedJob)
            .then((data) => {
                const baseStats = [data.hp, data.mp, data.atk, data.def];
                this.setState(state => {
                    state.stats = baseStats;
                    return state;
                })
            })
            .catch((err) => {
                console.error('err', err);
            });
    };

    render() {
        return (
            <div>
                <StatsTable stats={this.state.stats}/>
            </div>
        );
    }
}
