import React, { Component } from 'react';
import {fetchJobs, deleteJob} from '../../actions/jobActions';
import JobTable from '../../components/jobs/Table';

export default class ListJobs extends Component {

    constructor(props) {
        super(props);

        this.state = {
            jobs: []
        };
    };

    componentDidMount() {
        fetchJobs()
            .then((data) => {
                this.setState(state => {
                    state.jobs = data;
                    return state;
                })
            })
            .catch((err) => {
                console.error('err', err);
            });
    };

    onDelete(id) {
        deleteJob(id)
            .then((data) => {
                let jobs = this.state.jobs.filter((job) => {
                    return id !== job.id;
                });

                this.setState(state => {
                    state.jobs = jobs;
                    return state;
                });
            })
            .catch((err) => {
                console.error('err', err);
            });
    }

    render() {
        return (
            <div>
                <JobTable jobs={this.state.jobs}
                       onDelete={this.onDelete.bind(this)}
                />
            </div>
        );
    }
}
