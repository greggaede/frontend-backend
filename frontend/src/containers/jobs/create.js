import React, { Component } from 'react';
import { withRouter } from 'react-router';
import JobForm from '../../components/jobs/form';
import { createJob } from '../../actions/jobActions';

export default class CreateJob extends Component {

    handleSubmit(data) {
        this.props.router.push('/admin/jobs');
        createJob(data);
    }

    render() {
        return (
            <div>
                <JobForm onSubmit={this.handleSubmit.bind(this)}></JobForm>
            </div>
        );
    }
}
