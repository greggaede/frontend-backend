import React, { Component } from 'react';
import JobForm from '../../components/jobs/form';
import { fetchJob, updateJob } from '../../actions/jobActions';

const UpdateJob = React.createClass ({

    getInitialState() {
        return {
            job: {}
        };
    },

    componentDidMount() {
        fetchJob(this.props.params.jobId)
            .then((data) => {
                this.setState(state => {
                    state.job = data;
                    return state;
                });
            })
            .catch((err) => {
                console.error('err', err);
            });
    },

    handleSubmit(data) {
        updateJob(this.state.job.id, data);
        this.props.router.push('/admin/jobs');
    },

    render() {
        return (
            <div>
                <JobForm onSubmit={this.handleSubmit}
                      name={this.state.job.name}
                      description={this.state.job.description}
                      hp={this.state.job.hp}
                      mp={this.state.job.mp}
                      atk={this.state.job.atk}
                      def={this.state.job.def}>
                </JobForm>
            </div>
        );
    }
});

export default UpdateJob;
