import React, { Component } from 'react';
import {fetchMonsters, deleteMonster} from '../../actions/monsterActions';
import MonsterTable from '../../components/monsters/Table';

export default class ListMonsters extends Component {

    constructor(props) {
        super(props);

        this.state = {
            monsters: []
        };
    };

    componentDidMount() {
        fetchMonsters()
            .then((data) => {
                this.setState(state => {
                    state.monsters = data;
                    return state;
                })
            })
            .catch((err) => {
                console.error('err', err);
            });
    };

    onDelete(id) {
        deleteMonster(id)
            .then((data) => {
                let monsters = this.state.monsters.filter((monster) => {
                    return id !== monster.id;
                });

                this.setState(state => {
                    state.monsters = monsters;
                    return state;
                });
            })
            .catch((err) => {
                console.error('err', err);
            });
    }

    render() {
        return (
            <div>
                <MonsterTable monsters={this.state.monsters}
                       onDelete={this.onDelete.bind(this)}
                />
            </div>
        );
    }
}
