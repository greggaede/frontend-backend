import React, { Component } from 'react';
import MonsterForm from '../../components/monsters/form';
import { fetchMonster, updateMonster } from '../../actions/monsterActions';

const UpdateMonster = React.createClass ({

    getInitialState() {
        return {
            monster: {}
        };
    },

    componentDidMount() {
        fetchMonster(this.props.params.monsterId)
            .then((data) => {
                this.setState(state => {
                    state.monster = data;
                    return state;
                });
            })
            .catch((err) => {
                console.error('err', err);
            });
    },

    handleSubmit(data) {
        updateMonster(this.state.monster.id, data);
        this.props.router.push('/admin/monsters');
    },

    render() {
        return (
            <div>
                <MonsterForm onSubmit={this.handleSubmit}
                      name={this.state.monster.name}
                      hp={this.state.monster.hp}
                      mp={this.state.monster.mp}
                      atk={this.state.monster.atk}
                      def={this.state.monster.def}>
                </MonsterForm>
            </div>
        );
    }
});

export default UpdateMonster;
