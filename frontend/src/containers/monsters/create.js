import React, { Component } from 'react';
import { withRouter } from 'react-router';
import MonsterForm from '../../components/monsters/form';
import { createMonster } from '../../actions/monsterActions';

export default class CreateMonster extends Component {

    handleSubmit(data) {
        this.props.router.push('/admin/monsters');
        createMonster(data);
    }

    render() {
        return (
            <div>
                <MonsterForm onSubmit={this.handleSubmit.bind(this)}></MonsterForm>
            </div>
        );
    }
}
