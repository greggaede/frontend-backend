import React, { Component } from 'react';
import { withRouter } from 'react-router';
import ItemForm from '../../components/items/form';
import { createItem } from '../../actions/itemActions';

export default class CreateItem extends Component {

    handleSubmit(data) {
        this.props.router.push('/admin/items');
        createItem(data);
    }

    render() {
        return (
            <div>
                <ItemForm onSubmit={this.handleSubmit.bind(this)}></ItemForm>
            </div>
        );
    }
}
