import React, { Component } from 'react';
import {fetchItems, deleteItem} from '../../actions/itemActions';
import ItemTable from '../../components/items/Table';

export default class ListItems extends Component {

    constructor(props) {
        super(props);

        this.state = {
            items: []
        };
    };

    componentDidMount() {
        fetchItems()
            .then((data) => {
                this.setState(state => {
                    state.items = data;
                    return state;
                })
            })
            .catch((err) => {
                console.error('err', err);
            });
    };

    onDelete(id) {
        deleteItem(id)
            .then((data) => {
                let items = this.state.items.filter((item) => {
                    return id !== item.id;
                });

                this.setState(state => {
                    state.items = items;
                    return state;
                });
            })
            .catch((err) => {
                console.error('err', err);
            });
    }

    render() {
        return (
            <div>
                <ItemTable items={this.state.items}
                       onDelete={this.onDelete.bind(this)}
                />
            </div>
        );
    }
}
