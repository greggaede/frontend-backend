import React, { Component } from 'react';
import ItemForm from '../../components/items/form';
import { fetchItem, updateItem } from '../../actions/itemActions';

const UpdateItem = React.createClass ({

    getInitialState() {
        return {
            item: {}
        };
    },

    componentDidMount() {
        fetchItem(this.props.params.itemId)
            .then((data) => {
                this.setState(state => {
                    state.item = data;
                    return state;
                });
            })
            .catch((err) => {
                console.error('err', err);
            });
    },

    handleSubmit(data) {
        updateItem(this.state.item.id, data);
        this.props.router.push('/admin/items');
    },

    render() {
        return (
            <div>
                <ItemForm onSubmit={this.handleSubmit}
                      name={this.state.item.name}
                      description={this.state.item.description}
                      hp_raise={this.state.item.hp_raise}
                      mp_raise={this.state.item.mp_raise}
                      atk_raise={this.state.item.atk_raise}
                      def_raise={this.state.item.def_raise}>
                </ItemForm>
            </div>
        );
    }
});

export default UpdateItem;
