import React, { Component } from 'react';
import ListAvailableJobs from './listJobs';

export default class NewGame extends Component {

    render() {
        return (
            <div className="newGame">
                <h1 className="text-center">New Game</h1>
                <h2>Select a class:</h2>
                <ListAvailableJobs />
            </div>
        );
    }
}