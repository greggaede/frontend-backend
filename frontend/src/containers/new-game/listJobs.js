import React, { Component } from 'react';
import {fetchJobs} from '../../actions/jobActions';
import AvailableJobsTable from '../../components/jobs/AvailableJobsTable';

export default class ListAvailableJobs extends Component {

    constructor(props) {
        super(props);

        this.state = {
            jobs: []
        };
    };

    componentDidMount() {
        fetchJobs()
            .then((data) => {
                this.setState(state => {
                    state.jobs = data;
                    return state;
                })
            })
            .catch((err) => {
                console.error('err', err);
            });
    };

    render() {
        return (
            <div>
                <AvailableJobsTable jobs={this.state.jobs}/>
            </div>
        );
    }
}
